# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
{
  config,
  pkgs,
  inputs,
  outputs,
  ...
}: {
  imports = [
    # Include the results of the hardware scan.
    ./hardware-configuration.nix
    inputs.home-manager.nixosModules.default
  ];

  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true; # Open ports in the firewall for Steam Remote Play
    dedicatedServer.openFirewall = true; # Open ports in the firewall for Source Dedicated Server
  };

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "nixos"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  nix.settings.experimental-features = ["nix-command" "flakes"];

  powerManagement = {
    enable = true;
    powertop.enable = true;
  };

  # https://www.reddit.com/r/NixOS/comments/1712eoe/is_there_a_way_to_check_metricsheat_regarding_cpu/
  services = {
    auto-cpufreq.enable = true;
    thermald.enable = true;
  };
  # Suspend
  services.logind = {
    lidSwitch = "hibernate";
  };

  # In order to make hibernation work
  # https://discourse.nixos.org/t/btrfs-swap-not-enough-swap-space-for-hibernation/36805/4
  systemd.services.systemd-logind.environment = {
    SYSTEMD_BYPASS_HIBERNATION_MEMORY_CHECK = "1";
  };

  console.useXkbConfig = true;

  # Bluetooth
  hardware.bluetooth = {
    enable = true;
    powerOnBoot = true;
  };

  # Set your time zone.
  time.timeZone = "Africa/Casablanca";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_US.UTF-8";
    LC_IDENTIFICATION = "en_US.UTF-8";
    LC_MEASUREMENT = "en_US.UTF-8";
    LC_MONETARY = "en_US.UTF-8";
    LC_NAME = "en_US.UTF-8";
    LC_NUMERIC = "en_US.UTF-8";
    LC_PAPER = "en_US.UTF-8";
    LC_TELEPHONE = "en_US.UTF-8";
    LC_TIME = "en_US.UTF-8";
  };

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Enable the KDE Plasma Desktop Environment.
  #services.xserver.displayManager.sddm.enable = true;
  #services.xserver.desktopManager.plasma5.enable = true;

  services.xserver.displayManager.gdm.enable = true;
  services.xserver.desktopManager.gnome.enable = true;

  services.xserver.windowManager.qtile.enable = true;

  services.gnome.gnome-keyring.enable = true;

  environment.gnome.excludePackages =
    (with pkgs; [
      gnome-photos
      gnome-tour
    ])
    ++ (with pkgs.gnome; [
      cheese # webcam tool
      gnome-music
      gnome-terminal
      gedit # text editor
      epiphany # web browser
      geary # email reader
      evince # document viewer
      gnome-characters
      totem # video player
      tali # poker game
      iagno # go game
      hitori # sudoku game
      atomix # puzzle game
    ]);

  # Configure keymap in X11
  services.xserver = {
    xkb = {
      variant = "";
      options = "caps:swapescape,grp:alt_shift_toggle";
      layout = "us";
    };
  };

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable sound with pipewire.
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  virtualisation.libvirtd.enable = true;
  programs.virt-manager.enable = true;

  virtualisation.virtualbox.host.enable = true;

  virtualisation.docker.enable = true;

  services.fprintd = {
    enable = true;
    tod.enable = true;
    tod.driver = pkgs.libfprint-2-tod1-vfs0090;
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.achraf = {
    isNormalUser = true;
    description = "Achraf";
    extraGroups = ["networkmanager" "wheel" "video" "libvirtd" "wireshark" "docker"];
    shell = pkgs.fish;
    packages = with pkgs; [
      firefox
      # kate
      #  thunderbird
    ];
  };

  home-manager = {
    extraSpecialArgs = {inherit inputs;};
    users = {
      "achraf" = import ./home.nix;
    };
  };

  nixpkgs.overlays = [outputs.overlays.unstable-packages];

  nixpkgs.config = {
    # Allow unfree packages
    allowUnfree = true;

    # Workaround for Obsidian, Electron EOL
    permittedInsecurePackages = ["electron-25.9.0"];
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    vim
    neovim
    wget
    git
    alacritty
    kitty
    neofetch
    htop
    yt-dlp
    syncthing
    lf
    xclip

    qtile

    vscode

    vagrant
    unstable.ollama

    alejandra
    libnotify

    fprintd

    vscodium

    zellij
    starship
    du-dust
    duf
    bat
    lsd
    fd
    zoxide
    procs

    nerdfonts
    nordzy-icon-theme

    telegram-desktop
    discord
    brave
    chromium

    gparted
    wireshark

    vlc
    kdenlive
    drawio
    obs-studio

    onlyoffice-bin
    zathura
    evince
    xournalpp
    calibre
    newsboat

    bitwarden
    obsidian
    zotero
    slack

    gnome-extension-manager
    gnome.gnome-tweaks
    gnomeExtensions.blur-my-shell

    fishPlugins.done
    fishPlugins.fzf-fish
    fishPlugins.forgit
    fishPlugins.hydro
    fzf
    fishPlugins.grc
    grc
  ];

  xdg.mime.defaultApplications = {
    "application/pdf" = "org.gnome.Evince.desktop";
  };

  programs = {
    neovim = {
      enable = true;
      defaultEditor = true;
    };

    fish = {
      enable = true;
      shellAliases = {
        ll = "lsd -l";
        update = "sudo nixos-rebuild switch";
        v = "nvim";
        vf = "cd ~/Documents/vagrant_files";
      };
      interactiveShellInit = ''
               eval (zellij setup --generate-auto-start fish | string collect)
        starship init fish | source
      '';
    };
  };

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [8384 22000];
  networking.firewall.allowedUDPPorts = [22000 21027];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.11"; # Did you read the comment?
}
