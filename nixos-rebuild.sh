#!/usr/bin/env nix-shell
#! nix-shell -i bash -p bash
# A rebuild script that commits on a successful build
# inspired by 0atman
set -e

pushd ~/Documents/nixos-config/
nvim configuration.nix
alejandra . &>/dev/null
git diff -U0 *.nix
echo "NixOS Rebuilding..."
sudo nixos-rebuild switch --impure --flake ~/Documents/nixos-config/  || (cat nixos-switch.log | grep --color error && false)
# sudo nixos-rebuild switch --impure --flake ~/Documents/nixos-config/ &>nixos-switch.log || (cat nixos-switch.log | grep --color error && false)
echo "Done."
current=$(nixos-rebuild list-generations | grep current)
git commit -am "$current"
popd
notify-send -e "NixOS Rebuilt OK!" --icon=software-update-available
